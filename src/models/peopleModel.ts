export type PeopleModel = {
    name: string;
    height: string;
    mass: string;
    hair_color: string;
    skin_color: string;
    eye_color: string;
    birth_year: string;
    gender: string;
    homeworld: string;
    films: string[];
    species: string[];
    vehicles: string[];
    starships: string[];
    create: string;
    edited: string;
    url: string;
};

export type PeopleResultModel = {
    count: number;
    next: string | null;
    previus: string | null;
    results: PeopleModel[];
};
