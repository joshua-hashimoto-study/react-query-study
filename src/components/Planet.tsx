import React from "react";
import { PlanetModel } from "../models/planetModel";

type Props = {
    planet: PlanetModel;
};

const Planet = ({ planet }: Props) => {
    return (
        <div className="card">
            <h3>{planet.name}</h3>
            <p>Population - {planet.population}</p>
            <p>Terrain - {planet.terrain}</p>
        </div>
    );
};

export default Planet;
