import React from "react";
import { useQuery } from "react-query";
import { peopleAPI } from "../networking/api";
import Person from "./Person";

const People = () => {
    const fetchPeople = async () => {
        const { data } = await peopleAPI.people();
        return data;
    };

    const { data, status } = useQuery("people", fetchPeople);

    return (
        <div>
            <h2>People</h2>
            {status === "loading" && <div>Loading data...</div>}

            {status === "error" && <div>Error fetching data</div>}

            {status === "success" && (
                <div>
                    {data?.results.map((person) => {
                        return <Person key={person.name} person={person} />;
                    })}
                </div>
            )}
        </div>
    );
};

export default People;
