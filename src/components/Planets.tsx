import React, { useState } from "react";
import { useQuery } from "react-query";
import { PlanetResultModel } from "../models/planetModel";
import { planetAPI } from "../networking/api";
import Planet from "./Planet";

const Planets = () => {
    const [page, setPage] = useState(1);
    const fetchPlanets = async (pageNumber: number) => {
        const { data } = await planetAPI.planets(pageNumber);
        return data;
    };

    const { data, isLoading, error } = useQuery(
        ["planets", page],
        () => fetchPlanets(page),
        { keepPreviousData: true }
    );

    if (isLoading) {
        return <div>Loading data...</div>
    }

    if (error) {
        return <div>Error fetching data: {error.message}</div>
    }

    return (
        <div>
            <h2>Planets</h2>

            <button onClick={() => setPage(1)}>page 1</button>
            <button onClick={() => setPage(2)}>page 2</button>
            <button onClick={() => setPage(3)}>page 3</button>
            <div>
                {data?.results.map((planet) => {
                    return <Planet key={planet.name} planet={planet} />;
                })}
            </div>
        </div>
    );
};

export default Planets;
