import React from "react";
import { PeopleModel } from "../models/peopleModel";

type Props = {
    person: PeopleModel;
};

const Person = ({ person }: Props) => {
    return (
        <div className="card">
            <h3>{person.name}</h3>
            <p>Gender - {person.gender}</p>
            <p>Birth year - {person.birth_year}</p>
        </div>
    );
};

export default Person;
