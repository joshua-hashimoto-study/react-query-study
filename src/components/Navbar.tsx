import React from "react";

type Props = {
    setPage: (pageName: string) => void;
};

const Navbar = ({ setPage }: Props) => {
    return (
        <nav>
            <button onClick={() => setPage("planets")}>Planets</button>
            <button onClick={() => setPage("people")}>People</button>
        </nav>
    );
};

export default Navbar;
