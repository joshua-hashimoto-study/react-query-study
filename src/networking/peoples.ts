import { AxiosInstance, AxiosResponse } from "axios";
import { PeopleResultModel } from "./../models/peopleModel";

const peopleAPICalls = (client: AxiosInstance) => {
    return {
        async people(): Promise<AxiosResponse<PeopleResultModel>> {
            return await client.get("people/");
        },
    };
};

export default peopleAPICalls;
