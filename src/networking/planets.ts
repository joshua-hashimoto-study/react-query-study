import { AxiosInstance, AxiosResponse } from "axios";
import { PlanetResultModel } from "../models/planetModel";

const planetsAPICalls = (client: AxiosInstance) => {
    return {
        async planets(page: number): Promise<AxiosResponse<PlanetResultModel>> {
            return await client.get(`planets/?page=${page}`);
        },
    };
};

export default planetsAPICalls;
