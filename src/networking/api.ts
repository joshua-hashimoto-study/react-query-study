import axios from "axios";
import peopleAPICalls from "./peoples";
import planetsAPICalls from "./planets";

const baseURL = "http://swapi.dev/api/";

const client = axios.create({
    baseURL: baseURL,
});

const planetAPI = planetsAPICalls(client);
const peopleAPI = peopleAPICalls(client);

export { planetAPI, peopleAPI };
export default client;
